/**
 * User: Buro26 - Jesper
 * Date: 8-11-2018
 * Time: 17:23
 */
class Game {
    constructor() {
        const self = this;
        this.updatesPerSecond = 10;

        window.interval = setInterval(function () {
            game.gameLoop()
        }, 1000 / this.updatesPerSecond);

        self.calculateTotalBossDamage();
    }

    calculateTotalBossDamage() {
        var damage = 0;
        for (var i = 0; i < window.model.heros.length; i++) {
            damage += window.model.heros[i].bossDamage();
        }

        this.totalBossDamage = damage / this.updatesPerSecond;
    }

    bossDamage() {
        var nextHealth = window.model.boss.currentHealth() - this.totalBossDamage;
        if (nextHealth > 0) {
            window.model.boss.currentHealth(nextHealth);
        } else {
            window.model.player.experience(window.model.player.experience() + window.model.boss.experience());
            window.model.boss.newBoss();
        }
    }

    targetDamage() {
        var target = window.model.boss.target();
        var damage = window.model.boss.targetDamage() / this.updatesPerSecond;
        var heroes = window.model.heros;

        if (target < heroes.length) {
            var hero = heroes[target];
        } else {
            this.gameOver();
            return;
        }
        var nextHealth = hero.currentHealth() - damage;

        if (nextHealth <= 0) {
            window.model.boss.target(window.model.boss.target() + 1);
            nextHealth = 0;
        }
        hero.currentHealth(nextHealth);
    }

    playerRegen() {
        window.model.player.regen(this.updatesPerSecond);
    }

    gameOver() {
        alert('Gameover reloading, but you get your current level added to your starting points!');
        var newPoints = window.model.player.level() + parseInt(localStorage.getItem('statPoints'));
        localStorage.setItem('statPoints', newPoints);
        window.interval = clearInterval(window.interval);
        location.reload(true);
    }

    gameLoop() {
        this.bossDamage();
        this.targetDamage();
        this.playerRegen();
    };
}


