function AppViewModel() {
    this.boss = new Boss();
    this.level = ko.observable(1);
    this.heros = [
        new Tank('Tanky'),
        new Healer('Healy'),
        new Dps('Sparky'),
        new Dps('Stabby'),
        new Dps('Throwy'),
    ];

    this.castBar = new CastBar();
    this.spells = [
        new Heal('Heal', 'heal'),
        new Heal('Flash heal', 'flashHeal'),
        new Heal('Greater Heal', 'greaterHeal')
    ];
    this.player = new Player();
    this.message = new Message('', '');
}

function init() {
    window.model = new AppViewModel();
    window.game = new Game();
    ko.applyBindings(model);
}

init();


function random(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function hardReset() {
    localStorage.setItem('statPoints', 0);
    window.location.reload();
}
