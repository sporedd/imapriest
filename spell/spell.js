class Spell {
    constructor(name, manaCost, castTime, effectText) {
        this.name = name;
        this.manaCost = manaCost;
        this.castTime = castTime;
        this.effectText = effectText;
        this.target;

    }

    canCast() {
        this.target = window.model.heros[window.model.player.heroTarget];
        if (!this.target) {
            window.model.message.newMessage('error', 'No target');
            return false;
        }

        if (window.model.castBar.casting) {
            window.model.message.newMessage('error', 'Casting');
            return false;
        }

        if (window.model.player.currentMana() - this.manaCost <= 0) {
            window.model.message.newMessage('error', 'No enough mana');
            return false;
        }

        return true;
    }

    cast() {
        if (this.canCast()) {
            var castBar = window.model.castBar;
            var self = this;

            castBar.casting = true;
            var start = new Date().getTime();
            castBar.interval = setInterval(function () {
                var distance = new Date().getTime() - start;
                castBar.remainingTime(self.castTime * 1000 - distance);
                castBar.percentage(distance / (self.castTime * 10));
                if (distance >= self.castTime * 1000) {
                    castBar.reset();
                    window.model.player.currentMana(window.model.player.currentMana() - self.manaCost);
                    window.model.player.hinderRegen(true);
                    self.effect();
                }
            }, 10);
            castBar.name(this.name);
        }
    }
}