/**
 * User: Buro26 - Jesper
 * Date: 10-11-2018
 * Time: 15:17
 */

class Heal extends Spell {
    constructor(name, type) {
        let healing = ko.observable(100);
        let manaCost = 100;
        let castTime = 1;

        switch (type) {
            case 'greaterHeal' :
                manaCost = 400;
                healing = ko.observable(500);
                castTime = 5;
                break;
            case 'flashHeal' :
                manaCost = 200;
                healing = ko.observable(150);
                castTime = .5;
                break;
        }
        var effectText = ko.observable('Heals for ' + healing());

        super(name, manaCost, castTime, effectText);
        this.healing = healing;
    }

    effect() {
        var healing = Math.floor(this.healing() * window.model.player.spellPower() / 100);
        console.log(healing);
        var newHealth = this.target.currentHealth() + healing;
        if (newHealth < this.target.maxHealth()) {
            this.target.currentHealth(newHealth);
        } else {
            this.target.currentHealth(this.target.maxHealth());
        }
    }

}
