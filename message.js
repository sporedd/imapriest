/**
 * User: Buro26 - Jesper
 * Date: 10-11-2018
 * Time: 16:18
 */

class Message {
    constructor(type, html) {
        self = this;
        this.type = ko.observable(type);
        this.html = ko.observable(html);
    }

    newMessage(type, html) {
        this.type(type);
        this.html(html);

        document.getElementById('messageBox').style.opacity = 1;
        clearTimeout(this.timeOut);
        this.timeOut = setTimeout(function () {
            document.getElementById('messageBox').style.opacity = 0;
        }, 2000)
    }
}