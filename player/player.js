/**
 * User: Buro26 - Jesper
 * Date: 10-11-2018
 * Time: 15:44
 */

class Player {
    constructor() {
        var self = this;
        this.maxMana = ko.observable(2000);
        this.currentMana = ko.observable(2000);
        self.manaPercentage = ko.observable(self.calculateManaPercantage());

        this.baseRegen = ko.observable(20);
        this.currentRegen = ko.observable(this.baseRegen());
        this.combatRegen = ko.observable(20);
        this.halfRegen = ko.observable(false);

        this.spellPower = ko.observable(100);

        this.statPoints = ko.observable(2);
        if (localStorage.getItem('statPoints')) {
            this.statPoints(this.statPoints() + parseInt(localStorage.getItem('statPoints')));
        }

        this.stats = [
            new Stat('base-regen', 'Base regen', 'Increase base regen by a 1 * x', 0, 1000),
            new Stat('max-mana', 'Maximum mana', 'Increase maximum mana by 100 * x', 0, 1000),
            new Stat('combat-regen', 'Combat regen', 'Increase combat regen with 5 * x% of your base regen', 0, 10),
            new Stat('leader-ship', 'Leadership', 'Increases your parties damage with x', 0, 10),
            new Stat('spell-power', 'Spellpower', 'Increases your healing power with * x%', 0, 100),
        ];

        self.currentMana.subscribe(function () {
            self.manaPercentage(self.calculateManaPercantage());
        });
        self.halfRegen.subscribe(function () {
            self.calculateRegen();
        });

        self.baseRegen.subscribe(function () {
            self.calculateRegen();
        });

        this.experience = ko.observable(0);
        this.level = ko.observable(1);
        this.nextLevel = ko.observable(70);
        self.experiencePercentage = ko.observable(self.calculateExperiencePercantage());

        self.experience.subscribe(function () {
            self.experiencePercentage(self.calculateExperiencePercantage());
            if (self.experience() >= self.nextLevel()) {
                self.levelUp();
            }
        });

        this.heroTarget = 0;
    }

    calculateManaPercantage() {
        return Math.round(this.currentMana() / this.maxMana() * 100, 2);
    }

    calculateExperiencePercantage() {
        return Math.round(this.experience() / this.nextLevel() * 100, 2);
    }

    levelUp() {
        var curve = Math.pow(1.1, this.level());
        this.experience(this.experience() - this.nextLevel());
        this.level(this.level() + 1);
        this.nextLevel(Math.floor(this.nextLevel() * curve));
        this.statPoints(this.statPoints() + 5);
    }

    hinderRegen() {
        clearTimeout(this.hinderTimeout);
        this.halfRegen(true);
        self = this;
        this.hinderTimeout = setTimeout(function () {
            self.halfRegen(false);
        }, 3000)
    }

    calculateRegen() {
        let regen = this.baseRegen();
        if (this.halfRegen()) {
            regen = regen * (this.combatRegen() / 100);
        }
        this.currentRegen(Math.round(regen));
    }

    regen(updatesPerSecond) {
        var regen = this.currentMana() + (this.currentRegen() / updatesPerSecond);
        if (regen < this.maxMana()) {
            this.currentMana(Math.round(regen));
        } else {
            this.currentMana(this.maxMana());
        }
    }
}