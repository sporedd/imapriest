class Stat {
    constructor(techName, name, effect, value, maxValue) {
        this.techName = techName;
        this.name = ko.observable(name);
        this.effect = ko.observable(effect);
        this.value = ko.observable(value);
        this.maxValue = ko.observable(maxValue);
    }

    add() {
        var player = window.model.player;
        var statPoint = player.statPoints;

        if (statPoint() > 0 && this.value() < this.maxValue()) {
            this.value(this.value() + 1);
            statPoint(statPoint() - 1);
            switch (this.techName) {
                case 'base-regen': {
                    player.baseRegen(player.baseRegen() + 1);
                    break;
                }
                case 'max-mana': {
                    player.maxMana(player.maxMana() + 100);
                    break;
                }
                case 'combat-regen': {
                    player.combatRegen(Math.ceil(player.combatRegen() + 5));
                    break;
                }
                case 'leader-ship': {
                    for (var i = 0; i < window.model.heros.length; i++) {
                        window.model.heros[i].bossDamage(window.model.heros[i].bossDamage() + 1);
                    }
                    window.game.calculateTotalBossDamage();
                    break;
                }
                case 'spell-power': {
                    player.spellPower(player.spellPower() + 1);
                }
            }
        }
    }

    addFive() {
        for (var i = 0; i < 5; i++) {
            this.add();
        }
    }
}