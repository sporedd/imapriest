/**
 * User: Buro26 - Jesper
 * Date: 11-11-2018
 * Time: 17:15
 */

class CastBar {
    constructor() {
        self = this;
        this.name = ko.observable('');
        this.casting = false;
        this.remainingTime = ko.observable('');
        this.percentage = ko.observable(0);

        jQuery(document).keydown(function (e) {
            if (e.keyCode === 27) {
                window.model.message.newMessage('warning', 'Spell canceled');
                window.model.castBar.reset();
            }
        });
    }

    reset() {
        this.name('');
        this.casting = false;
        this.remainingTime('');
        this.percentage(0);
        clearInterval(this.interval);
    }
}