/**
 * User: Buro26 - Jesper
 * Date: 8-11-2018
 * Time: 15:37
 */

class Boss extends Enemy {
    constructor() {
        var startingHealth = 100;
        super('dummy', startingHealth);
        this.generateName();
        this.startingDamage = 20;
        this.startingHealth = startingHealth;
        this.targetDamage = ko.observable(20);
        this.target = ko.observable(0);
        this.startingExperience = 20;
        this.experience = ko.observable(this.startingExperience);
    }

    generateName() {
        const nameParts = ['ame', 'tal', 'ver', 'lel', 'non', 'gron', 'lest'];
        const titles = ['the terrible', 'the funny', 'the grey', 'the angry', 'the conqorer', 'the placer', 'the grinder'];
        let name = nameParts[random(0, nameParts.length)] + nameParts[random(0, nameParts.length)] + ' ' + titles[random(0, titles.length)];
        this.name(name[0].toUpperCase() + name.substring(1));
    }

    newBoss() {
        window.model.level(window.model.level() + 1);
        var curve = Math.pow(1.1, window.model.level());
        this.generateName();
        var newHealth = Math.floor(this.startingHealth * curve);
        this.maxHealth(newHealth);
        this.targetDamage(Math.floor(this.startingDamage * curve));
        this.currentHealth(newHealth);
        this.experience(Math.floor(this.startingExperience * curve));
    }
}