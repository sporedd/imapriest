/**
 * User: Buro26 - Jesper
 * Date: 8-11-2018
 * Time: 15:38
 */

class Hero extends Unit {
    constructor(name, maxHealth, bossDamage) {
        super(name, maxHealth);
        this.bossDamage = ko.observable(bossDamage);
    }

    setTargeted() {
        for (var i = 0; i < window.model.heros.length; i++) {
            window.model.heros[i].targeted(false);
        }
        window.model.player.heroTarget = window.model.heros.indexOf(this);
        this.targeted(true);
    }
}