/**
 * User: Buro26 - Jesper
 * Date: 8-11-2018
 * Time: 15:38
 */

class Tank extends Hero {
    constructor(name) {
        super(name, 2000, 5);
    }
}