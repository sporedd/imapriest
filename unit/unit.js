/**
 * User: Buro26 - Jesper
 * Date: 8-11-2018
 * Time: 15:33
 */

class Unit {
    constructor(name, maxHealth) {
        const self = this;
        self.maxHealth = ko.observable(maxHealth);
        self.currentHealth = ko.observable(maxHealth);
        self.name = ko.observable(name);
        self.healthPercentage = ko.observable(self.calculateHealthPercantage());
        self.healthColor = ko.observable(self.calculateHealthColor());
        self.targeted = ko.observable(false);

        self.currentHealth.subscribe(function () {
            self.healthPercentage(self.calculateHealthPercantage());
            self.healthColor(self.calculateHealthColor());
        });
    }

    calculateHealthPercantage() {
        return Math.round(this.currentHealth() / this.maxHealth() * 100, 2);
    }

    calculateHealthColor() {
        if (this.healthPercentage() > 66) {
            return 'green';
        } else if (this.healthPercentage() > 33) {
            return 'orange';
        } else if (this.healthPercentage() > 0) {
            return 'red';
        } else {
            return 'dark-grey';
        }
    }

}